require 'json'
require 'pry'
require 'ruby-prof'
require 'semver_dialects'

sample = JSON.parse(File.read('sample.json'))

RubyProf.start

sample.each do |row|
  RubyProf.pause
  x = row.fetch('interval')
  y = row.fetch('lowest')
  z = row.fetch('highest')
  RubyProf.resume

  interval = VersionParser.parse(x)

  range = VersionRange.new
  range.add(VersionParser.parse(y))
  range.add(VersionParser.parse(z))

  !range.overlaps_with?(interval)
end

result = RubyProf.stop

File.open("result.html", 'w') do |file|
  printer = RubyProf::GraphHtmlPrinter.new(result)
  printer.print(file)
end
