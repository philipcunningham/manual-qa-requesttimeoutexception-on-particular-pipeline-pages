# bench

Example:

```sh
bundle install
RUBY_PROF_MEASURE_MODE=memory bundle exec ruby bench.rb
open result.html
```

These other profiling modes are available:

```sh
RUBY_PROF_MEASURE_MODE=wall
RUBY_PROF_MEASURE_MODE=process
RUBY_PROF_MEASURE_MODE=allocations
```
